# static-website-boilerplate
静的Webサイト構築用ボイラープレートです。主に以下の機能を実装しています。
- HTML/CSS/JSのコンポーネント・フレームワーク
- 開発用仮想サーバ(Browser Sync)とNunjucks、Sass(SCSS)、JavaScript(ES6)の自動コンパイル

## インストール
利用するにはNode.jsとUNIXシェルが必要です。Node.jsをインストールしたら、適当な場所に設置します。
ターミナルで設置したディレクトリに移動し、次のコマンドを実行して下さい。<br>
Nodeモジュールがインストールされ、用意されたスクリプトが利用できるようになります。
```shell
npm install
```
yarnも使えます。
```shell
yarn install
```

## 構成
| ディレクトリ/ファイル | 役割 |
|:--|:--|
| src | 起点となるディレクトリ。このディレクトリにソースを配置し、編集する |
| src/_lib | コンポーネント等ライブラリの保管場所 |
| dest | ローカルサーバのトップディレクトリ(≠ルート)。`WATCH`コマンドや`DEST`コマンドの実行で作成される |
| build | `BUILD`コマンドの実行で作成され、配布用に整形されたファイルが格納される。 |
| rsrc | Favicon等のテンプレートデータの保管場所 |
| node_modules | インストールされたNodeモジュールの保管場所 |
| package.json | プロジェクトで使用するモジュール、スクリプト、設定を記述するファイル |
| .vscode | VS Codeを利用する場合、中のtasks.jsonが読み込まれる |

## スクリプト
スクリプトは`package.json`に記述されており、以下のスクリプト名で利用できます。

| スクリプト名 | 動作 |
|:--:|:--|
| WATCH | srcディレクトリを監視する。中のファイルに変更を加えると自動的にコンパイルし、destディレクトリに出力。 |
| LIVE | Browser Syncでローカルサーバを起ち上げ、destディレクトリを監視する。ファイルに変更があれば即座にブラウザに反映される。`WATCH`コマンドと併用する。 |
| DEST | srcディレクトリ内のファイルをコンパイルし、destディレクトリに出力。 |
| BUILD | 配布用に整形したファイルを作成する。srcディレクトリ内のファイルをコンパイルし、buildディレクトリに出力。 |

`WATCH`、`DEST`、`BUILD`は、出力先のディレクトリに画像などのファイル自動的にコピーします。<br>
出力先のディレクトリは、`WATCH`及び`DEST`が**destディレクトリ**、`BUILD`が**buildディレクトリ**です。<br>
コピーは`rsync`を使って行われ、次の条件に当てはまるファイルはコピーの対象外として設定されています。

- .(ドット)で始まるファイルとディレクトリ [`.*`]
- _(アンダースコア)で始まるファイルとディレクトリ [`_*`]
- HTMLファイル [`*.html`]
- SCSSファイル [`*.scss`]

HTMLとSCSSはコンパイルの対象のためコピーしません。JSもコンパイルの対象ですが、jQuery等のファイルをコピーするために対象としています。そのため、JSのエントリーファイルにはアンダースコアをつけてコピーの対象外とする必要があります。

### スクリプトの実行
次のように入力して実行します。
```shell
npm run WATCH
```
yarnの場合は以下です。
```shell
yarn run WATCH
```

### スクリプトの停止
スクリプトは`control + c`で停止できます。
`WATCH`と`LIVE`の監視を停止したい場合等に使います。

### スクリプトの動作を変更する
スクリプトは、一部の動作の設定を変更することができます。

#### package.json > config
ディレクトリのパスやJavaScriptのエントリーファイル名などを変更したい時は、`package.json`を編集して下さい。
```json:packgage.json
"config": {
  "SRC_DIR": "src", //ソースを配置するディレクトリの名前
  "DEST_DIR": "dest", //ローカルサーバのトップディレクトリの名前
  "BUILD_DIR": "build", //配布用ディレクトリの名前
  "CSS_DIR": "css", //CSSを配置するディレクトリの名前
  "JS_DIR": "js", //JavaScriptを配置するディレクトリの名前
  "JS_ENTRY_FILE": "_main.js", //JavaScriptのエントリーファイル名
  "JS_OUT_FILE": "main.js", //JavaScriptのビルドファイル名
  "COPY_FILES": "**/!(_*.*|*.html|*.scss)", //スクリプト実行時にコピーするファイルの指定
  "AUTOPREFIXER_CONFIG": "1%, last 2 versions, Firefox ESR, ie >= 9, Android >= 4, ios_saf >= 8" //Autoprefixerの対象ブラウザの指定
},
```

呼び出すには、次のように記述します。
```json:package.json
$npm_package_config_SRC_DIR
```

#### Autoprefixer
前述の通り、`package.json`の`AUTOPREFIXER_CONFIG`でAutoprefixerの対象ブラウザを変更できます。
対象ブラウザの選定には[Browserlist](http://browserl.ist/?q=%3E+1%25%2C+last+2+versions%2C+Firefox+ESR%2C+ie+%3E%3D+9%2C+Android+%3E%3D+4%2C+ios_saf+%3E%3D+8)が便利です。

##### `WATCH`スクリプトのAutoprefixerについて
Autoprefixerはコンパイルが遅いため、`WATCH`スクリプトでは動作しません。
開発時にAutoprefixerが必要な場合は、`package.json`の`_watch:css-autoprefix`からアンダースコアを取り除くか、任意のタイミングで`DEST`スクリプトを実行して下さい。


#### Babel Presets
Babelの設定は、`.babelrc`ではなく`package.json`に記述しています。
```json:package.json
"babel": {
  "presets": [
    [
      "env",
      {
        "targets": {
          "browsers": [ //対象ブラウザの指定
            "last 2 versions",
            "Firefox ESR",
            "ie >= 9",
            "Android >= 4",
            "ios_saf >= 9"
          ]
        }
      }
    ]
  ],
  "plugins": [
    "transform-runtime"
  ]
},
```


### VS Codeでの利用
#### 任意のスクリプトを実行する
`cmd + shift + p`でコマンドを呼び出し、`タスクの実行`を選びます。任意のスクリプトを選択して下さい。

#### キーボードショートカット
`keybinding.json`に以下を追加してキーボードショートカットを利用できます。<br>
srcディレクトリに新しいファイルを追加した時など、`WATCH`スクリプトの再実行が必要な場合に便利です。
```json:keybinding.json
{
  "key": "alt+w",
  "command": "workbench.action.tasks.runTask",
  "args": "npm: WATCH"
},
{
  "key": "alt+l",
  "command": "workbench.action.tasks.runTask",
  "args": "npm: LIVE"
},
{
  "key": "alt+d",
  "command": "workbench.action.tasks.runTask",
  "args": "npm: DEST"
},
{
  "key": "alt+b",
  "command": "workbench.action.tasks.runTask",
  "args": "npm: BUILD"
}
```
