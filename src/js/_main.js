// import {Test} from '../_lib/js/_Test.js'; // eslint-disable-line no-unused-vars
import {Dev} from '../_lib/js/_Dev.js';
import {AutoWrapElem} from '../_lib/js/_AutoWrapElem.js';
import {ScrollBox} from '../_lib/js/_ScrollBox.js';

window.writingArea = '.writing';
window.overrideClass = '.override';

$(function() {
  'use strict';
  const dev = new Dev(); // eslint-disable-line no-unused-vars
  const embeds = new AutoWrapElem(); // eslint-disable-line no-unused-vars
  const scrollBox = new ScrollBox(); // eslint-disable-line no-unused-vars

  //
  // タッチデバイス判定
  // const isTouchDevice = isTouchDeviceFn();
  // function isTouchDeviceFn() {
  //   if (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch) {
  //     $('html').addClass('is-touchDevice');
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
  //
  // iOS判定
  const is_iOS = isiOS();
  function isiOS() {
    const is_iOS = /iP(hone|ad|od)/.test(navigator.userAgent);
    return is_iOS;
  }
  //
  // メニューボタン
  $('#MainMenu_Overlay').on('click', function() {
    $('html').removeClass('mainMenu-open');
  });
  $('#MainMenuButton').on('click', function() {
    if ($('html').hasClass('mainMenu-open')) {
      mainMenuClose();
    } else {
      mainMenuOpen();
    }
  });
  // resize
  var resizeTimer = false;
  var windowWidth = $(window).width();
  $(window).on('resize orientationchange', function() {
    if (resizeTimer !== false) {
      clearTimeout(resizeTimer);
    }
    resizeTimer = setTimeout(function() {
      if (windowWidth !== $(window).width()) {
        $('html').removeClass('mainMenu-open');
        windowWidth = $(window).width();
      }
    }, 100);
  });
  // オーバーレイのスクロール無効
  let supportsPassive = false;
  try {
    /* eslint-disable indent */
    let opt = {
      get passive() {
        supportsPassive = true;
      }
    }, handler = function() {};
    /* eslint-enable indent */
    window.addEventListener('checkpassive', handler, opt);
    window.removeEventListener('checkpassive', handler, opt);
  } catch (err) {
    // error
  }
  const addEventListenerWithOptions = (target, type, handler, options) => {
    let optionsOrCapture = options;
    if (!supportsPassive) {
      optionsOrCapture = options.capture;
    }
    target.addEventListener(type, handler, optionsOrCapture);
  };
  const overlayContent = document.querySelector('#MainMenu_Overlay');
  // overlayContent.addEventListener('touchmove', function(event) {
  addEventListenerWithOptions(overlayContent, 'touchmove', function(event) {
    event.preventDefault();
  }, {passive: false});
  //
  /* Reference: https://qiita.com/noraworld/items/2834f2e6f064e6f6d41a */
  const scrollContent = document.querySelector('#MainMenu_Contents');
  const scrollContentBody = document.querySelector('#mainMenu_Body');
  scrollContent.scrollTop = 2;

  // window.addEventListener('touchmove', function(event) {
  addEventListenerWithOptions(window, 'touchmove', function(event) {
    if ($('html').hasClass('mainMenu-open')) {
      if (scrollContent.scrollTop !== 0 && scrollContent.scrollTop + scrollContent.clientHeight !== scrollContent.scrollHeight) {
        event.stopPropagation();
      } else if (scrollContentBody.clientHeight < document.documentElement.clientHeight) {
        event.preventDefault();
      }
    }
  }, {passive: false});
  if (is_iOS) {
    $('html').addClass('is-iOS');
    scrollContent.addEventListener('scroll', function() {
      if (scrollContent.scrollTop === 0) {
        scrollContent.scrollTop = 1;
      } else if (scrollContent.scrollTop + scrollContent.clientHeight === scrollContent.scrollHeight) {
        scrollContent.scrollTop = scrollContent.scrollTop - 2;
      }
    });
  }
  const mainMenuOpen = () => {
    $('html').addClass('mainMenu-open');
    scrollContent.scrollTop = 1;
  };
  const mainMenuClose = () => {
    $('html').removeClass('mainMenu-open');
    scrollContent.scrollTop = 1;
  };
});