export class AutoWrapElem {
  constructor(writingArea, obj, disabledClass) {
    this.writingArea = writingArea;
    this.obj = obj;
    this.autoWrapDisabledClass = disabledClass;
    this.defaultObj = {
      map: {
        selector: 'iframe[src*="www.google.com/maps/embed"]',
        className: 'ratio',
        wrapElem: 'div',
        appendClass: ''
      },
      video: {
        selector: 'iframe[src*="www.youtube.com/embed"]',
        className: 'ratio',
        wrapElem: 'div',
        appendClass: ''
      },
      scrollBox: {
        selector: 'table',
        className: 'scrollBox',
        wrapElem: 'div',
        appendClass: ''
      }
    };
    this.init(this.writingArea, this.obj, this.disabledClass);
  }
  init(_writingArea = window.writingArea, _obj = this.defaultObj, _disabledClass = 'autoWrap-disabled') {
    Object.keys(_obj).forEach(function(item) {
      var _thisObj = this;
      $(_thisObj[item].selector, _writingArea).each(function() {
        if (!$(this).parent().hasClass(_thisObj[item].className) && !$(this).hasClass(_disabledClass)) {
          $(this).wrap(() => {
            var _wrapElem = `<${_thisObj[item].wrapElem} class="${_thisObj[item].className} ${_thisObj[item].appendClass}" />`;
            return _wrapElem;
          });
        }
      });
    }, _obj);
  }
}