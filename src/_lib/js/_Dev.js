export class Dev {
  constructor() {
    this.init();
  }
  init() {
    var _this = this;
    $('[data-dev]').each(function(i) {
      // var _name = $(this).data('dev');
      var _class = _this.genClassSwitch($(this));
      var _devTool = `<div id="DevTool-${i}" class="devTool override">${_class}</div>`;
      $(this).attr('data-dev', i).before(_devTool);
      _this.applySwitch(i);
    });
  }
  genClassSwitch(elem) {
    var _class = elem.data('class');
    var _switch = '';
    if (_class) {
      _class.forEach(function(value) {
        var _checked = '';
        if (elem.hasClass(value)) {
          _checked = 'checked';
        }
        _switch += `<label><input type="checkbox" value="${value}" ${_checked}>&nbsp;${value}</label>`;
      });
    }
    return _switch;
  }
  applySwitch(index) {
    $('body').on('change', `#DevTool-${index} input`, function() {
      var $target = $(`[data-dev=${index}]`);
      if ($(this).prop('checked')) {
        // $(this).closest('.devTool').next().addClass($(this).attr('value'));
        $target.addClass($(this).attr('value'));
      } else {
        $target.removeClass($(this).attr('value'));
      }
    });
  }
}