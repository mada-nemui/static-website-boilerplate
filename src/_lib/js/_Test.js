export class Test {
  constructor(text) {
    this.text = text;
    this.display(this.text);
  }
  display(text) {
    console.log('Test: ' + text);
  }
}