export class ScrollBox {
  constructor() {
    this.target = '.scrollBox';
    this.readyClass = 'scrollBox-ready';
    this.readyActivatePlay = 5; // 数値分の差ではreadyにさせない(IE対応)
    this.init();
  }
  init(_target = this.target, _readyClass = this.readyClass) {
    var _this = this;
    var timer = false;
    $(window).resize(function() {
      if (timer !== false) {
        clearTimeout(timer);
      }
      timer = setTimeout(function() {
        _this.resize();
      }, 100);
    });
    _this.resize();
    $(_target).on('scroll', function() {
      $(this).removeClass(_readyClass);
    });
  }
  resize(_target = this.target, _readyClass = this.readyClass) {
    var _this = this;
    $(_target).each(function() {
      var boxWidth = $(this).outerWidth();
      var contentWidth = $(this).children().outerWidth();
      if ((contentWidth - _this.readyActivatePlay) > boxWidth) {
        $(this).addClass(_readyClass);
      } else {
        $(this).removeClass(_readyClass);
      }
    });
  }
}